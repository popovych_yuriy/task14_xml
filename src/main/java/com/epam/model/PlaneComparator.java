package com.epam.model;

import java.util.Comparator;

public class PlaneComparator implements Comparator<Plane> {
    @Override
    public int compare(Plane o1, Plane o2) {
        if (o1 == o2) {
            return 0;
        }
        if (o1 == null) {
            return -1;
        }
        if (o2 == null) {
            return 1;
        }
        return o1.getModel().compareTo(o2.getModel());
    }
}
