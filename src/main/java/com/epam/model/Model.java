package com.epam.model;

import java.io.File;

public interface Model {
    void printByDom();

    void collectByDom();

    void printBySax();

    void collectBySax(File xml, File xsd);

    void printByStax(File xml);

    void collectByStax(File xml);

    void convertToHtml();

    void createXml(File xml);
}
