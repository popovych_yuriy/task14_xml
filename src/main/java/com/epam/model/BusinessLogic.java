package com.epam.model;

import com.epam.parser.dom.DomParser;
import com.epam.parser.dom.DomXmlConverter;
import com.epam.parser.sax.SaxParser;
import com.epam.parser.sax.SaxPrinter;
import com.epam.parser.stax.StaxParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

public class BusinessLogic implements Model {
    private DomParser domParser;
    private SaxPrinter saxPrinter;
    private SaxParser saxParser;
    private StaxParser staxParser;


    public BusinessLogic() {
    }

    @Override
    public void printByDom() {
        try {
            domParser = new DomParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        domParser.printElements(domParser.getNodeList(), 0);
    }

    @Override
    public void collectByDom() {
        try {
            domParser = new DomParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        domParser.xmlToCollection().forEach(System.out::println);
    }

    @Override
    public void printBySax() {
        try {
            saxPrinter = new SaxPrinter();
            saxPrinter.printElements();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void collectBySax(File xml, File xsd) {
        try {
            saxParser = new SaxParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        saxParser.parsePlanes(xml, xsd).forEach(System.out::println);
    }

    @Override
    public void printByStax(File xml) {
        staxParser = new StaxParser();
        try {
            staxParser.printConsole(xml);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void collectByStax(File xml) {
        try {
            staxParser = new StaxParser();
            staxParser.parse(xml).forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void convertToHtml() {
        try {
            DomXmlConverter converter = new DomXmlConverter();
            domParser = new DomParser();
            converter.listToXml(domParser.xmlToCollection());
            FileInputStream xml = new FileInputStream("src/main/resources/xml/output.xml");
            FileInputStream xsl = new FileInputStream("src/main/resources/xml/planes.xsl");
            Source xmlDoc = new StreamSource(xml);
            Source xslDoc = new StreamSource(xsl);

            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer trans = factory.newTransformer(xslDoc);
            trans.transform(xmlDoc, new StreamResult(new FileOutputStream("src/main/resources/xml/output.html")));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createXml(File xml) {
        DomXmlConverter converter = new DomXmlConverter();
        try {
            domParser = new DomParser();
            converter.listToXml(domParser.xmlToCollection());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}
