package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    File xml = new File("src/main/resources/xml/planes.xml");
    File xsd = new File("src/main/resources/xml/planes.xsd");
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        try {
            controller = new ControllerImpl();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Read XML and print by DOM");
        menu.put("2", "  2 - Collect to list and print by DOM");
        menu.put("3", "  3 - Read XML and print by SAX");
        menu.put("4", "  4 - Collect to list and print by SAX");
        menu.put("5", "  5 - Read XML and print by STAX");
        menu.put("6", "  6 - Collect to list and print by STAX");
        menu.put("7", "  7 - Create XML sorted by model");
        menu.put("8", "  8 - Convert to HTML sorted by model");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::printByDom);
        methodsMenu.put("2", this::collectByDom);
        methodsMenu.put("3", this::printBySax);
        methodsMenu.put("4", this::collectBySax);
        methodsMenu.put("5", this::printByStax);
        methodsMenu.put("6", this::collectByStax);
        methodsMenu.put("7", this::createXml);
        methodsMenu.put("8", this::convertHtml);
    }

    private void convertHtml() {
        controller.convertToHtml();
        System.out.println("New output.html is creating in resource/xml folder.");
    }

    private void createXml() {
        controller.createXml(xml);
        System.out.println("New output.xml is creating in resource/xml folder.");
    }

    private void collectByStax() {
        controller.collectByStax(xml);
    }

    private void printByStax() {
        controller.printByStax(xml);
    }

    private void collectBySax() {
        controller.collectBySax(xml, xsd);
    }

    private void printBySax() {
        controller.printBySax();
    }

    private void collectByDom() {
        controller.collectByDom();
    }

    private void printByDom() {
        controller.printByDom();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Wrong input!");
            }
        } while (!keyMenu.equals("Q"));
    }
}
