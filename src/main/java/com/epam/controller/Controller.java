package com.epam.controller;

import java.io.File;

public interface Controller {
    void printByDom();

    void collectByDom();

    void printBySax();

    void collectBySax(File xml, File xsd);

    void printByStax(File xml);

    void collectByStax(File xml);

    void createXml(File xml);

    void convertToHtml();
}
