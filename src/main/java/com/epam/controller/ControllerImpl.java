package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() throws IOException, SAXException, ParserConfigurationException {
        model = new BusinessLogic();
    }

    @Override
    public void printByDom() {
        model.printByDom();
    }

    @Override
    public void collectByDom() {
        model.collectByDom();
    }

    @Override
    public void printBySax() {
        model.printBySax();
    }

    @Override
    public void collectBySax(File xml, File xsd) {
        model.collectBySax(xml, xsd);
    }

    @Override
    public void printByStax(File xml) {
        model.printByStax(xml);
    }

    @Override
    public void collectByStax(File xml) {
        model.collectByStax(xml);
    }

    @Override
    public void createXml(File xml) {
        model.createXml(xml);
    }

    @Override
    public void convertToHtml() {
        model.convertToHtml();
    }
}