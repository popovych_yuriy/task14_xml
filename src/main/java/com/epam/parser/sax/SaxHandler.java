package com.epam.parser.sax;

import com.epam.model.Plane;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    private List<Plane> planes = new ArrayList<>();
    private Plane plane = null;
    private boolean bModel = false;
    private boolean bCountry = false;
    private boolean bRole = false;
    private boolean bCrew = false;
    private boolean bRockets = false;
    private boolean bRadar = false;
    private boolean bLength = false;
    private boolean bWingspan = false;
    private boolean bHeight = false;
    private boolean bPrice = false;

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equalsIgnoreCase("plane")) {
            String planeId = attributes.getValue("planeID");
            plane = new Plane();
            plane.setPlaneId(Integer.parseInt(planeId));
        } else if (qName.equalsIgnoreCase("model")) {
            bModel = true;
        } else if (qName.equalsIgnoreCase("country")) {
            bCountry = true;
        } else if (qName.equalsIgnoreCase("role")) {
            bRole = true;
        } else if (qName.equalsIgnoreCase("crew")) {
            bCrew = true;
        } else if (qName.equalsIgnoreCase("rockets")) {
            bRockets = true;
        } else if (qName.equalsIgnoreCase("radar")) {
            bRadar = true;
        } else if (qName.equalsIgnoreCase("length")) {
            bLength = true;
        } else if (qName.equalsIgnoreCase("wingspan")) {
            bWingspan = true;
        } else if (qName.equalsIgnoreCase("height")) {
            bHeight = true;
        } else if (qName.equalsIgnoreCase("price")) {
            bPrice = true;
        }
    }

    public void characters(char[] ch, int start, int length) {
        if (bModel) {
            plane.setModel(new String(ch, start, length));
            bModel = false;
        } else if (bCountry) {
            plane.setCountry(new String(ch, start, length));
            bCountry = false;
        } else if (bRole) {
            plane.setRole(new String(ch, start, length));
            bRole = false;
        } else if (bCrew) {
            plane.setCrew(Integer.parseInt(new String(ch, start, length)));
            bCrew = false;
        } else if (bRockets) {
            plane.setRockets(Integer.parseInt(new String(ch, start, length)));
            bRockets = false;
        } else if (bRadar) {
            plane.setRadar(Boolean.parseBoolean(new String(ch, start, length)));
            bRadar = false;
        } else if (bLength) {
            plane.setLength(Double.parseDouble(new String(ch, start, length)));
            bLength = false;
        } else if (bWingspan) {
            plane.setWingspan(Double.parseDouble(new String(ch, start, length)));
            bWingspan = false;
        } else if (bHeight) {
            plane.setHeight(Double.parseDouble(new String(ch, start, length)));
            bHeight = false;
        } else if (bPrice) {
            plane.setPrice(Integer.parseInt(new String(ch, start, length)));
            bPrice = false;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("plane")) {
            planes.add(plane);
        }
    }

    public List<Plane> getPlanes() {
        return planes;
    }
}
