package com.epam.parser.sax;

import com.epam.model.Plane;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser {
    private SAXParserFactory factory = SAXParserFactory.newInstance();
    private SAXParser parser = factory.newSAXParser();
    private SaxHandler handler = new SaxHandler();
    private SaxValidator validator = new SaxValidator();
    private List<Plane> planes;

    public SaxParser() throws ParserConfigurationException, SAXException {
    }

    public List<Plane> parsePlanes(File xml, File xsd) {
        planes = new ArrayList<>();
        try {
            factory.setSchema(validator.createSchema(xsd));
            parser.parse(xml, handler);
            planes = handler.getPlanes();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return planes;
    }
}
