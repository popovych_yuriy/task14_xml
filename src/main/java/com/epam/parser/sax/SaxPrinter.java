package com.epam.parser.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

public class SaxPrinter {
    private SAXParserFactory factory = SAXParserFactory.newInstance();
    private SAXParser parser = factory.newSAXParser();
    private DefaultHandler handler;

    public SaxPrinter() throws ParserConfigurationException, SAXException {
    }

    public void printElements() throws ParserConfigurationException, SAXException, IOException {
        SaxPrinter saxPrinter = new SaxPrinter();
        saxPrinter.handler = new DefaultHandler() {
            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                String name = attributes.getValue("planeID");
                if (name != null && !name.isEmpty()) {
                    System.out.println("planeID = " + name);
                }
            }

            @Override
            public void characters(char[] ch, int start, int length) throws SAXException {
                String str = "";
                for (int i = 0; i < length; i++) {
                    str += ch[start + i];
                }
                str = str.trim();
                str = str.replaceAll("(?m)^[ \t]*\r?\n", "");
                System.out.println(str);
            }
        };
        saxPrinter.parser.parse(new File("src/main/resources/xml/planes.xml"), saxPrinter.handler);
    }
}
