package com.epam.parser.sax;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class SaxValidator {
    private Schema schema = null;
    private String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
    private SchemaFactory factory;

    public Schema createSchema(File xsd) throws SAXException {
        factory = SchemaFactory.newInstance(language);
        schema = factory.newSchema(xsd);
        return schema;
    }
}
