package com.epam.parser.dom;

import com.epam.model.Plane;
import com.epam.model.PlaneComparator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

public class DomXmlConverter {
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder builder;
    private Document document;

    public void listToXml(List<Plane> planes) {
        Collections.sort(planes, new PlaneComparator());
        try {
            builder = factory.newDocumentBuilder();
            document = builder.newDocument();
            Text text;

            document.setXmlStandalone(true);
            ProcessingInstruction pi = document.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"planes.xsl\"");
            Element root = document.createElement("planes");
            document.appendChild(root);
            document.insertBefore(pi, root);
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:noNamespaceSchemaLocation", "planes.xsd");

            for (Plane actual : planes) {
                Element plane = document.createElement("plane");
                root.appendChild(plane);
                plane.setAttribute("planeID", String.valueOf(actual.getPlaneId()));
                Element model = document.createElement("model");
                plane.appendChild(model);
                text = document.createTextNode(actual.getModel());
                model.appendChild(text);
                Element country = document.createElement("country");
                plane.appendChild(country);
                text = document.createTextNode(actual.getCountry());
                country.appendChild(text);
                Element role = document.createElement("role");
                plane.appendChild(role);
                text = document.createTextNode(actual.getRole());
                role.appendChild(text);
                Element crew = document.createElement("crew");
                plane.appendChild(crew);
                text = document.createTextNode(String.valueOf(actual.getCrew()));
                crew.appendChild(text);
                Element rockets = document.createElement("rockets");
                plane.appendChild(rockets);
                text = document.createTextNode(String.valueOf(actual.getRockets()));
                rockets.appendChild(text);
                Element radar = document.createElement("radar");
                plane.appendChild(radar);
                text = document.createTextNode(String.valueOf(actual.isRadar()));
                radar.appendChild(text);
                Element parameters = document.createElement("parameters");
                plane.appendChild(parameters);
                Element length = document.createElement("length");
                parameters.appendChild(length);
                text = document.createTextNode(String.valueOf(actual.getLength()));
                length.appendChild(text);
                Element wingspan = document.createElement("wingspan");
                parameters.appendChild(wingspan);
                text = document.createTextNode(String.valueOf(actual.getWingspan()));
                wingspan.appendChild(text);
                Element height = document.createElement("height");
                parameters.appendChild(height);
                text = document.createTextNode(String.valueOf(actual.getHeight()));
                height.appendChild(text);
                Element price = document.createElement("price");
                plane.appendChild(price);
                text = document.createTextNode(String.valueOf(actual.getPrice()));
                price.appendChild(text);
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(new DOMSource(document), new StreamResult(new FileOutputStream("src/main/resources/xml/output.xml")));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
