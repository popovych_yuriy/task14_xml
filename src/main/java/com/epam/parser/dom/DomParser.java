package com.epam.parser.dom;

import com.epam.model.Plane;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomParser {
    private DomParser domParser;
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private DocumentBuilder builder = factory.newDocumentBuilder();
    private Document document = builder.parse(new File("src/main/resources/xml/planes.xml"));
    private Element element = document.getDocumentElement();
    private NodeList nodeList = element.getChildNodes();

    public DomParser() throws ParserConfigurationException, IOException, SAXException {
    }

    private static String getTab(int tabs) {
        String str = "";
        for (int i = 0; i < tabs; i++) {
            str += "\t";
        }
        return str;
    }

    public NodeList getNodeList() {
        return nodeList;
    }

    public void printElements(NodeList nodeList, int tabs) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i) instanceof Element) {
                String value = "";
                if (!nodeList.item(i).getTextContent().trim().isEmpty()
                        && !((Text) nodeList.item(i).getFirstChild()).getData().trim().isEmpty()
                        && !((Text) nodeList.item(i).getFirstChild()).getData().trim().equals("\n")) {
                    Text text = (Text) nodeList.item(i).getFirstChild();
                    value += " = " + text.getData().trim();
                }
                System.out.println(getTab(tabs) + nodeList.item(i).getNodeName() + value);

                if (((Element) nodeList.item(i)).hasAttribute("planeID")) {
                    System.out.println("planeID: " + ((Element) nodeList.item(i)).getAttribute("planeID"));
                }
                if (nodeList.item(i).hasChildNodes()) {
                    printElements(nodeList.item(i).getChildNodes(), ++tabs);
                }
            }
        }
    }

    public List<Plane> xmlToCollection() {
        nodeList = element.getChildNodes();
        List<Plane> planes = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Plane plane = new Plane();
            Node node = nodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                plane.setPlaneId(Integer.parseInt(element.getAttribute("planeID")));
                plane.setModel(element.getElementsByTagName("model").item(0).getTextContent());
                plane.setCountry(element.getElementsByTagName("country").item(0).getTextContent());
                plane.setRole(element.getElementsByTagName("role").item(0).getTextContent());
                plane.setCrew(Integer.parseInt(element.getElementsByTagName("crew").item(0).getTextContent()));
                plane.setRockets(Integer.parseInt(element.getElementsByTagName("rockets").item(0).getTextContent()));
                plane.setRadar(Boolean.parseBoolean(element.getElementsByTagName("radar").item(0).getTextContent()));
                plane.setLength(Double.parseDouble(element.getElementsByTagName("length").item(0).getTextContent()));
                plane.setWingspan(Double.parseDouble(element.getElementsByTagName("wingspan").item(0).getTextContent()));
                plane.setHeight(Double.parseDouble(element.getElementsByTagName("height").item(0).getTextContent()));
                plane.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()));
                planes.add(plane);
            }
        }
        return planes;
    }
}
