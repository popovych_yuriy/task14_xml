package com.epam.parser.stax;

import com.epam.model.Plane;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {
    private XMLInputFactory factory = XMLInputFactory.newInstance();
    private XMLStreamReader parser;
    private XMLEventReader reader;
    private XMLEvent event;
    private List<Plane> planes = new ArrayList<>();
    private Plane plane = null;

    public void printConsole(File xml) throws FileNotFoundException, XMLStreamException {
        parser = factory.createXMLStreamReader(new FileInputStream(xml));
        while(parser.hasNext()){
            int event =  parser.next();
            if(event == XMLStreamConstants.START_ELEMENT){
                System.out.println(parser.getLocalName());
            }
            if(event == XMLStreamConstants.CHARACTERS){
                System.out.println(parser.getText());
            }
        }
    }
    public List<Plane> parse(File xml) throws FileNotFoundException, XMLStreamException {
        reader = factory.createXMLEventReader(new FileInputStream(xml));
        while(reader.hasNext()){
            event = reader.nextEvent();
            if(event.isStartElement()){
                StartElement startElement = event.asStartElement();
                String name = startElement.getName().getLocalPart();
                switch (name){
                    case "plane":
                        plane = new Plane();
                        Attribute attribute = startElement.getAttributeByName(new QName("planeID"));
                        if(attribute != null){
                            plane.setPlaneId(Integer.parseInt(attribute.getValue()));
                        }
                        break;
                    case "model":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setModel(event.asCharacters().getData());
                        break;
                    case "country":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setCountry(event.asCharacters().getData());
                        break;
                    case "role":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setRole(event.asCharacters().getData());
                        break;
                    case "crew":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setCrew(Integer.parseInt(event.asCharacters().getData()));
                        break;
                    case "rockets":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setRockets(Integer.parseInt(event.asCharacters().getData()));
                        break;
                    case "radar":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setRadar(Boolean.parseBoolean(event.asCharacters().getData()));
                        break;
                    case "length":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setLength(Double.parseDouble(event.asCharacters().getData()));
                        break;
                    case "wingspan":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setWingspan(Double.parseDouble(event.asCharacters().getData()));
                        break;
                    case "height":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setHeight(Double.parseDouble(event.asCharacters().getData()));
                        break;
                    case "price":
                        event = reader.nextEvent();
                        assert plane !=null;
                        plane.setPrice(Integer.parseInt(event.asCharacters().getData()));
                        break;
                }
            }
            if(event.isEndElement()){
                EndElement endElement = event.asEndElement();
                if(endElement.getName().getLocalPart().equals("plane")){
                    planes.add(plane);
                }
            }
        }
        return planes;
    }

    public static void main(String[] args) {
        StaxParser staxParser = new StaxParser();
        File xml = new File("src/main/resources/xml/planes.xml");
        try {
            staxParser.parse(xml).forEach(System.out::println);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
