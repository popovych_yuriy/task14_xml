<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>My Planes</title>
            </head>
            <body>
                <h1>Military Planes</h1>
                <table style="width:100%;" border="1">
                    <tr>
                        <th>Model</th>
                        <th>Country</th>
                        <th>Role</th>
                        <th>Crew</th>
                        <th>Rockets</th>
                        <th>Radar</th>
                        <th>Length</th>
                        <th>Wingspan</th>
                        <th>Height</th>
                        <th>Price</th>
                    </tr>
                    <xsl:for-each select="/planes/plane">
                        <tr>
                            <td>
                                <xsl:value-of select="model"/>
                            </td>
                            <td>
                                <xsl:value-of select="country"/>
                            </td>
                            <td>
                                <xsl:value-of select="role"/>
                            </td>
                            <td>
                                <xsl:value-of select="crew"/>
                            </td>
                            <td>
                                <xsl:value-of select="rockets"/>
                            </td>
                            <td>
                                <xsl:value-of select="radar"/>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/length"/>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/wingspan"/>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/height"/>
                            </td>
                            <td>
                                <xsl:value-of select="price"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>